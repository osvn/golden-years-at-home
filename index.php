<?php
global $osvn_opt;
get_header();?>
<div class="full">
    <div class="container">
        <div id="index">
            <?php 
                if ( have_posts() ) // Neu co bai viet
    			{
    				while ( have_posts() ) : the_post();

    					get_template_part( 'content' );

    				endwhile;
    			}else{
    				get_template_part( 'content', 'none' );
    			}
            ?>
        </div>                      
    </div><!---.container-->
</div><!---.full-->
<?php get_footer();?>