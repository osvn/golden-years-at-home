jQuery(window).bind("load", function() {
	jQuery('.navbar-main').height(jQuery('body').height());
	jQuery('.navbar-toggle').on( "click", function(){
		if (jQuery('.navbar-main').hasClass('active'))
			jQuery('.navbar-main').removeClass('active');
		else
			jQuery('.navbar-main').addClass('active');
	});
	jQuery('.navbar-main .close-btn').click(function(){
		jQuery('.navbar-main').removeClass('active');
		jQuery('#navbar').removeClass('in');
	});

	jQuery('#customer_login .form-register').click(function(){
		jQuery('#customer_login .col-login').hide();
		jQuery('#customer_login .col-register').show();
	});
});