<?php
/**
 * The Template for displaying product category, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>
	<div id="main" class="full">
		<div class="container">
			<div class="content after-clear">
				<?php get_sidebar();?><!--/#sidebar -->
				<div id="content" class="fr">
					<?php
						/**
						 * woocommerce_before_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
					<?php if ( have_posts() ) : ?>
					<!--<div class="fran-fav after-clear">-->
						<?php 
						global $wp_query;
						if ( $wp_query->max_num_pages > 1 ) {
						?>
						<nav class="woocommerce-pagination top">
							<?php
								echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
									'base'         => esc_url( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
									'format'       => '',
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'total'        => $wp_query->max_num_pages,
									'prev_text'    => '&larr;',
									'next_text'    => '&rarr;',
									'type'         => 'list',
									'end_size'     => 3,
									'mid_size'     => 3
								) ) );
							?>
						</nav>
						<?php }?>


						<?php woocommerce_product_loop_start(); ?>

							<?php woocommerce_product_subcategories(); ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php wc_get_template_part( 'content', 'product' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>
						<?php 
						if ( $wp_query->max_num_pages > 1 ) {
						?>
						<nav class="woocommerce-pagination bottom">
							<?php
								echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
									'base'         => esc_url( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
									'format'       => '',
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'total'        => $wp_query->max_num_pages,
									'prev_text'    => '&larr;',
									'next_text'    => '&rarr;',
									'type'         => 'list',
									'end_size'     => 3,
									'mid_size'     => 3
								) ) );
							?>
						</nav>
						<?php }?>

					<!--</div><!--/.fran-fav -->
					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

						<?php wc_get_template( 'loop/no-products-found.php' ); ?>

					<?php endif; ?>
				</div>
			</div><!--/#content -->
		</div>
	</div><!--/#main -->
<?php get_footer( 'shop' ); ?>