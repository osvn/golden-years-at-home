<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>
	<div id="main" class="full">
		<div class="container">
			<div class="content after-clear">
				<?php get_sidebar();?><!--/#sidebar -->
				<div id="content" class="fr">
					<?php
						/**
						 * woocommerce_before_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
					<?php 
					/*
					* @get product category
					*/
					$args = array(
						    'orderby'           => 'id', 
						    'order'             => 'ASC',
						    'hide_empty'        => false, 
						); 
					$terms = get_terms( 'product_cat', $args );
					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
					?>
					<ul class="cat-list">
						<?php foreach ( $terms as $term ) {?>
						<?php 
						$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
					    $image = wp_get_attachment_url( $thumbnail_id );
						?>
						<li><a href="<?php echo get_term_link( $term );?>">
								<?php if ( $image ) {?>
								<span class="hover"><img src="<?php echo $image;?>" class="res-img" alt="<?php echo $term->name;?>"></span>
								<?php }?>
								<h4><?php echo $term->name;?></h4>
							</a></li>
						<?php }?>
					</ul><!--/.cat-list -->
					<?php }?>

					<?php if ( have_posts() ) : ?>
					<div class="fran-fav after-clear">
						<h4 class="title"><span><img src="<?php echo OSVN_IMG;?>/user.jpg" alt="" class="res-img"></span>FRAN'S FAVORITES</h4>
						<?php woocommerce_product_loop_start(); ?>

							<?php woocommerce_product_subcategories(); ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php wc_get_template_part( 'content', 'product' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>
					</div><!--/.fran-fav -->
					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

						<?php wc_get_template( 'loop/no-products-found.php' ); ?>

					<?php endif; ?>
				</div>
			</div><!--/#content -->
		</div>
	</div><!--/#main -->
<?php get_footer( 'shop' ); ?>