<?php get_header();?>
<div id="wrapper_repeat">
        	<div id="wrapper_margin">
            	<div id="mainbody_left_repeat">
                	<div id="mainbody_left_bottom">
                    	
                    	<?php get_sidebar('left');?>
						
                        <div id="right_col">
							
							<div id="main_content_full">

                           		<div id="left_box">
                                	<div id="left_box_padding">
                                    	<?php 
	                                    	while(have_posts()){the_post();
	                                    		echo '<h1 class="blog_post_heading">'.get_the_title().'</h1><hr>';
	                                    		if(has_post_thumbnail()){
	                                    			the_post_thumbnail();
	                                    			echo '<hr>';
	                                    		}
	                                    		the_content();
	                                    	}
                                    	?>
                                    </div><!---#left_box_padding-->
                                </div><!---#left_box-->

							</div><!---#main_content-->

						</div><!---#right_col-->
                    </div><!---#mainbody_left_bottom-->
                </div><!---#mainbody_left_repeat-->
            </div><!---#wrapper_margin-->
        </div><!---#wrapper_repeat-->
<?php get_footer();?>