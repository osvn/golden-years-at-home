<?php
global $osvn_opt;
?>
	<!DOCTYPE html>
	<!--[if lt IE 7 ]>
	<html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
	<!-- the "no-js" class is for Modernizr. -->

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">

		<title><?php osvn_title(); ?></title>

		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<![endif]-->
		<?php
		if ( is_search() ) {
			echo '<meta name="robots" content="noindex, nofollow" />';
		}
		?>

		<meta name="title" content="<?php bloginfo( 'name' ); ?>">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>"/>
		<meta name="Copyright" content="Copyright &copy; <?php bloginfo( 'name' ); ?> <?php echo date( 'Y' ); ?>. All Rights Reserved.">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

		<?php wp_head(); ?>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
		<script><?php if(isset($osvn_opt['header_script']) && !empty($osvn_opt['header_script'])){echo $osvn_opt['header_script'];} ?></script>
	</head>

<body cz-shortcut-listen="true" <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
<div id="wrapper">
	<header>
		<div class="container relative">
			<?php if(function_exists('osvn_logo')){osvn_logo();}?>
			<?php 
			global $current_user;
      		get_currentuserinfo();
			if($current_user->ID == ''){
			?>
				<a href="" class="login fl" data-toggle="modal" data-target="#login">LOGIN</a>
			<?php }else{?>
				<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="login fl">PROFILE</a>
			<?php }?>
			<div class="right fr text-right">
				<?php get_search_form();?>
				<?php if(function_exists('is_cart')){?>
					<?php global $woocommerce; ?>
					<a href="" class="cart-contents cart inline-block"  data-toggle="modal" data-target="#your-cart"><?php echo $woocommerce->cart->get_cart_total(); ?></a><br/>
				<?php }?>
				<?php if(function_exists('osvn_hotline')){osvn_hotline();}?>
			</div>
		</div>
	</header>

	

    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
		<div class="navbar-main">
			<div class="close-btn hidden-lg hidden-md"><span class="glyphicon glyphicon-remove"></span> CLOSE</div>
			<div id="navbar" aria-expanded="false">
				<?php
					// Main Menu
					wp_nav_menu(array(
						'theme_location' => 'primary',
						'menu_class' => 'animation-all menu',
						'container' => false,
						'menu_id' => 'main-menu',
					));
				?>
			</div><!--/.navbar-collapse -->
		</div>
      </div>
    </nav>