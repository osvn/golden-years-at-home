<?php
/*
Template name: Home
*/
?>
<?php get_header();?>
<?php global $post;?>
<div class="full">
	<div class="container">
		<?php if(function_exists('osvn_slider')){osvn_slider();}?>
		<!--#slider-->
			<div id="index">
			<?php while(have_posts()): the_post();?>
				<div class="fran-fav after-clear">
					<?php the_content();?>
				</div>

				<?php 
				$bottom = get_post_meta(get_the_ID(), '_osvn_home_box_des', true);
				if(isset($bottom) && !empty($bottom)){
				?>
				<div class="box-avatar fr">
					<?php 
					$img = get_post_meta(get_the_ID(), '_osvn_home_box_image_id', true);
					echo wp_get_attachment_image( $img, 'home-blog', null, array(
						 'class' => 'avatar res-img',
						 ) );
					?>
					<h4><?php echo get_post_meta(get_the_ID(), '_osvn_home_box_title', true);?></h4>
					<?php echo wpautop($bottom);?>
				</div><!--.box-avatar-->
				<?php }?>

				<div class="clear"></div>
			<?php endwhile;?>
			</div><!--#index-->
		</div>
	</div>
<?php get_footer();?>