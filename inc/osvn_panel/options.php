<?php
// elusive icons webfont demo: http://shoestrap.org/downloads/elusive-icons-webfont/

$this->sections[] = array(
	'icon'   => 'el-icon-cogs',
	'title'  => __( 'General Settings', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'       => 'logo',
			'type'     => 'media',
			'title'    => __( 'Logo', 'redux-framework-demo' ),
		),
		array(
			'id'        => 'hotline',
			'type'      => 'text',
			'title'     => __('Hotline', 'osvn'),
			'default'   => '1.855.249.8444',
		),
		array(
			'id'        => 'hours-open',
			'type'      => 'text',
			'title'     => __('Hours of operation', 'osvn'),
			'default'   => '9h to 17h',
		),
		array(
			'id'       => 'header_script',
			'type'     => 'ace_editor',
			'mode'     => 'javascript',
			'title'    => __( 'Header Scipt', 'redux-framework-demo' ),
		),
		array(
			'id'       => 'footer_brand',
			'type'     => 'editor',
			'title'    => __( 'Footer brand', 'redux-framework-demo' ),
			'default'  => '<a href="http://www.graco.com/au/en.html" target="_blank"><img src="'.OSVN_IMG.'/graco.jpg" alt="" /></a><a href="http://www.masterbuilders.asn.au/" target="_blank"><img src="'.OSVN_IMG.'/master_builders.jpg" alt="" /></a><a href="http://www.pda-online.org/" target="_blank"><img src="'.OSVN_IMG.'/pda.jpg" alt="" /></a>',
		),
		array(
			'id'       => 'footer_text',
			'type'     => 'editor',
			'title'    => __( 'Footer Text', 'redux-framework-demo' ),
			'default'  => 'Copyright &copy; 2014 Liquimix. &nbsp; All Rights Reserved &nbsp; | &nbsp; <a href="http://localhost/liquimix/contact-us-pg24941">Contact</a> &nbsp; | &nbsp; <a href="http://localhost/liquimix/Latest-News">Articles</a>',
		),
		array(
			'id'       => 'footer_script',
			'type'     => 'ace_editor',
			'mode'     => 'javascript',
			'title'    => __( 'Footer Scipt', 'redux-framework-demo' ),
		),

	)
);
$this->sections[] = array(
	'icon'   => 'el-icon-idea',
	'title'  => __( 'Advertise Settings', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'       => 'adv-header',
			'type'     => 'media',
			'title'    => __( 'Advertise header', 'redux-framework-demo' ),
		),
		array(
			'id'        => 'adv-header-link',
			'type'      => 'text',
			'title'     => __('Link', 'osvn'),
		),

	)
);
$this->sections[] = array(
	'icon'      => 'el-icon-share',
	'title'     => __('Social', 'redux-framework-demo'),
	'fields'    => array(
		array(
			'id'        => 'gplus',
			'type'      => 'text',
			'title'     => __('Google Plus', 'osvn'),
			'default'   => '+Username',
		),
		array(
			'id'        => 'facebook',
			'type'      => 'text',
			'title'     => __('Facebook ID', 'osvn'),
			'default'   => 'username',
		),
		array(
			'id'        => 'twitter',
			'type'      => 'text',
			'title'     => __('Twitter ID', 'osvn'),
			'default'   => 'username',
		),
	)
);



$theme_info = '<div class="redux-framework-section-desc">';
$theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __( '<strong>Theme URL:</strong> ', 'redux-framework-demo' ) . '<a href="' . $this->theme->get( 'ThemeURI' ) . '" target="_blank">' . $this->theme->get( 'ThemeURI' ) . '</a></p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __( '<strong>Author:</strong> ', 'redux-framework-demo' ) . $this->theme->get( 'Author' ) . '</p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __( '<strong>Version:</strong> ', 'redux-framework-demo' ) . $this->theme->get( 'Version' ) . '</p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get( 'Description' ) . '</p>';
$tabs = $this->theme->get( 'Tags' );
if ( ! empty( $tabs ) ) {
	$theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __( '<strong>Tags:</strong> ', 'redux-framework-demo' ) . implode( ', ', $tabs ) . '</p>';
}
$theme_info .= '</div>';

if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
	$this->sections['theme_docs'] = array(
		'icon'   => 'el-icon-list-alt',
		'title'  => __( 'Documentation', 'redux-framework-demo' ),
		'fields' => array(
			array(
				'id'       => '17',
				'type'     => 'raw',
				'markdown' => true,
				'content'  => file_get_contents( dirname( __FILE__ ) . '/../README.md' )
			),
		),
	);
}
$this->sections[] = array(
	'title'  => __( 'Import / Export', 'redux-framework-demo' ),
	'desc'   => __( 'Import and Export your Redux Framework settings from file, text or URL.', 'redux-framework-demo' ),
	'icon'   => 'el-icon-refresh',
	'fields' => array(
		array(
			'id'         => 'opt-import-export',
			'type'       => 'import_export',
			'title'      => 'Import Export',
			'subtitle'   => 'Save and restore your Redux options',
			'full_width' => false,
		),
	),
);

$this->sections[] = array(
	'type' => 'divide',
);

$this->sections[] = array(
	'icon'   => 'el-icon-info-sign',
	'title'  => __( 'Theme Information', 'redux-framework-demo' ),
	'desc'   => __( '<p class="description">This is the Description. Again HTML is allowed</p>', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'      => 'opt-raw-info',
			'type'    => 'raw',
			'content' => $item_info,
		)
	),
);