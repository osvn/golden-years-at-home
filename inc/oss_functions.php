<?php
function osvn_custom_dashboard_widgets() {
	global $wp_meta_boxes;

	wp_add_dashboard_widget( 'custom_help_widget', 'Welcome', 'custom_dashboard_help' );
}

function custom_dashboard_help() {
	?>
	<p class="about-description"><?php _e( 'This website was designed and developed by ' ); ?><a
			href="http://creativehaus.com/" target="_blank"><?php echo _e( 'Creativehaus.com' ); ?></a></p>
	<p></p>
	<p><a href="http://creativehaus.com/" target="_blank"><img
				src="<?php echo OSVN_IMG . '/logo-creativehaus.png'; ?>"></a></p>
	<p></p>
	<div class="">
		<p class="about-description">If you are currently on, or interested in maintenance, please contact us.</p>

		<p><strong>Contact Number: 1-888-966-7265. Support:</strong> <a href="http://creativehaus.com/contact/"
		                                                                target="_blank">Click here</a></p>
	</div>
<?php
}

function osvn_title() {
	global $paged, $s;
	if ( ! function_exists( 'wpseo_init' ) ) {
		if ( function_exists( 'is_tag' ) && is_tag() ) {
			single_tag_title( __( 'Tag Archive for &quot;', 'osvn' ) );
			_e( '&quot; - ', 'osvn' );
		} elseif ( is_archive() ) {
			wp_title();
			_e( ' Archive - ', 'osvn' );
		} elseif ( is_search() ) {
			_e( 'Search for &quot;', 'osvn' ) . htmlspecialchars( $s ) . _e( '&quot; - ', 'osvn' );
		} elseif ( ! ( is_404() ) && ( is_single() ) || ( is_page() ) ) {
			wp_title();
			_e( ' - ', 'osvn' );
		} elseif ( is_404() ) {
			_e( 'Not Found - ', 'osvn' );
		}

		if ( is_home() ) {
			bloginfo( 'name' );
			_e( ' - ', 'osvn' );
			bloginfo( 'description' );
		} else {
			bloginfo( 'name' );
		}

		if ( $paged > 1 ) {
			_e( ' - page ', 'osvn') . $paged; }
	} else {
		wp_title();
	}
}
/*=================logo=============================*/
function osvn_logo(){
    global $osvn_opt;
    if(isset($osvn_opt['logo']['url']) && !empty($osvn_opt['logo']['url'])){
    	echo '<a href="'.esc_url( home_url( '/' ) ).'" class="logo absolute"><img src="'.$osvn_opt['logo']['url'].'" alt="" class="res-img"/></a>';
    }else{
    	echo '<a href="'.esc_url( home_url( '/' ) ).'" class="logo absolute"><img src="'.OSVN_IMG.'/logo.png" alt="" class="res-img" /></a>';
    }
    
}
/*=================phone=============================*/
function osvn_hotline(){
	global $osvn_opt;
	if(isset($osvn_opt['hotline']) && !empty($osvn_opt['hotline'])){
		$string = $osvn_opt['hotline'];
		$pattern = '/[^0-9]*/';
		$result = preg_replace($pattern,'', $string);
		echo '<a class="hotline inline-block" href="tel:'.$result.'">'.$string.'</a>';
	}
	if(isset($osvn_opt['hours-open']) && !empty($osvn_opt['hours-open'])){
		echo '<span>Hours of operation: <strong>'.$osvn_opt['hours-open'].'</strong></span>';		
	}
}
/*=================email=============================*/
function osvn_email(){
	global $osvn_opt;
	if(isset($osvn_opt['email-contact']) && !empty($osvn_opt['email-contact'])){
		echo '<a href="mailto:'.$osvn_opt['email-contact'].'" id="email"><img src="'.OSVN_IMG.'/email.jpg" alt="" /></a>';
	}
}
/*=================adv=============================*/
function osvn_advertise(){
	global $osvn_opt;
	if(isset($osvn_opt['adv-header']['url']) && !empty($osvn_opt['adv-header']['url'])){
		echo '<a class="pull-right adv" href="'.$osvn_opt['adv-header-link'].'" target="_blank"><img src="'.$osvn_opt['adv-header']['url'].'"></a>';
	}
}
/*==================slider==========================================*/
function osvn_slider(){
	$args = array('post_type'=>'osvn_slide');
	$wq = new WP_Query($args);
	if($wq->have_posts()){
		echo '
		<div id="slider">
			<div id="carousel" class="carousel slide" data-ride="carousel">
		';
			echo '<ol class="carousel-indicators animation-all">';
				$a = 0;
				while($wq->have_posts()){ $wq->the_post();
					if($a == 0){
						$active_a = 'active';
					}else{
						$active_a = '';
					}
					echo '<li data-target="#carousel" data-slide-to="'.$a.'" class="'.$active_a.'"></li>';
					$a++;
				}
			echo '</ol>';
			echo '<div class="carousel-inner" role="listbox">';
			$b = 0;
			while($wq->have_posts()){ $wq->the_post();
				if($b == 0){
					$active = 'active';
				}else{
					$active = '';
				}
				$link = get_post_meta(get_the_ID(), '_osvn_slider_link', true);
				if(isset($link) && !empty($link)){
					$img = '<a href="'.$link.'">'.get_the_post_thumbnail(get_the_ID(), 'slider').'</a>';
				}else{
					$img = get_the_post_thumbnail(get_the_ID(), 'slider');
				}
				echo '<div class="item '.$active.'">';
					echo $img;
				echo '</div>';
				$b++;
			}
			echo '</div>';
		echo '
			</div>
		</div>
		';
	}wp_reset_query();
}
/*==================get style,pattern,color... product====================*/
function osvn_get_term($taxonomy, $tax_name){
	$args = array(
						    'orderby'           => 'id', 
						    'order'             => 'ASC',
						    'hide_empty'        => false, 
						);
	$terms = get_terms( $taxonomy, $args );
	//var_dump($terms);
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		echo '
		<h4>'.$tax_name.'</h4>
		<ul class="list">
		';
		foreach ( $terms as $term ) {
			echo '<li><a href="' . get_term_link( $term ) . '">' . $term->name . ' (' . $term->count . ')</a></li>';
		}
		echo '
		</ul>
		';
	}

}
/*==========================login with email or user name=====================================*/
function login_with_email_address($username) {
        $user = get_user_by('email',$username);
        if(!empty($user->user_login))
                $username = $user->user_login;
        return $username;
}
add_action('wp_authenticate','login_with_email_address');
/*===================add class widget====================*/
function slug_widget_order_class() {
	global $wp_registered_sidebars, $wp_registered_widgets;

	// Grab the widgets
	$sidebars = wp_get_sidebars_widgets();

	if ( empty( $sidebars ) )
	return;

	// Loop through each widget and change the class names
	foreach ( $sidebars as $sidebar_id => $widgets ) {
		if ( empty( $widgets ) )
		continue;
		$number_of_widgets = count( $widgets );
		foreach ( $widgets as $i => $widget_id ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' widget-order-' . $i;

		// Add first widget class
		if ( 0 == $i ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' first_main_menu_item selected_main_menu_item';
		}

		// Add last widget class
		if ( $number_of_widgets == ( $i + 1 ) ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' last_main_menu_item';
		}
		}
	}
}
add_action( 'init', 'slug_widget_order_class' );
/*===================woocommerce==========================*/
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	
	ob_start();
	
	?>
	<a href="" class="cart-contents cart inline-block"  data-toggle="modal" data-target="#your-cart"><?php echo $woocommerce->cart->get_cart_total(); ?></a>
	<?php
	
	$fragments['a.cart-contents'] = ob_get_clean();
	
	return $fragments;
	
}
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' > ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}
if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
	function woocommerce_template_loop_product_thumbnail() {
	    global $post;
	    if ( has_post_thumbnail() )
	          echo get_the_post_thumbnail( $post->ID, 'product-thumb', array('class'=>'res-img') );
	}
}
function mp1403200959() {

	if ( is_shop() ) {
		
		return 3;
		
	} else {
		
		return get_option('posts_per_page');
		
	}
}
if( class_exists( 'WooCommerce' ) ) :
	remove_filter( 'loop_shop_per_page', 'hamburg_wc_loop_shop_per_page', 11 );
	add_filter( 'loop_shop_per_page', 'mp1403200959', 11 );
endif;
?> 