<?php 
// Creating the widget 
class OSVN_Widget_Child_Page extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'OSVN_Widget_Child_Page', 

// Widget name will appear in UI
__('* OSVN Widget Child Page', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'OSVN widget list child page', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
$osvn_page_id = $instance['select-page'];
// before and after widget arguments are defined by themes
echo $args['before_widget'];

// This is where you run the code and display the output
?>
	<?php 
	$osvn_wp_page = new WP_Query();
	$all_wp_pages = $osvn_wp_page->query(array('post_type' => 'page'));
	$osvn_get_page = get_page_children( $osvn_page_id, $all_wp_pages );
	//var_dump($osvn_get_page);
	?>
	<a href="<?php echo get_permalink($osvn_page_id);?>"><?php echo get_the_title($osvn_page_id);?></a>
	<?php if($osvn_get_page){?>
	<ul id="submenu_6818">
		<?php foreach($osvn_get_page as $page_child){?>
		<li class="cat_id_<?php echo $page_child->ID;?>"><a href="<?php echo get_permalink($page_child->ID);?>"><?php echo get_the_title($page_child->ID);?></a></li>
		<?php }?>
	</ul>
	<?php }?>
<?php
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Latest News', 'osvn' );
}
// Widget admin form
/////////////////////////////////////////
    $tt_pages = array();
    $tt_pages_obj = get_pages('sort_column=post_parent,menu_order');    
    //////////////////////////////////////////
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<label for="<?php echo $this->get_field_id( 'select-page' ); ?> "><?php _e('Select page', 'example'); ?></label>
<select id="<?php echo $this->get_field_id( 'select-page' ); ?>" name="<?php echo $this->get_field_name( 'select-page' ); ?>">
     <?php foreach ($tt_pages_obj as $tt_page) {?>
     <option value="<?php echo $tt_page->ID;?>" <?php if($instance[ 'select-page' ] == $tt_page->ID){echo 'selected="selected"';}?>><?php echo $tt_page->post_title;?></option>
     <?php }?>
</select>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['select-page'] = ( ! empty( $new_instance['select-page'] ) ) ? strip_tags( $new_instance['select-page'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function osvn_load_child_page_widget() {
	register_widget( 'OSVN_Widget_Child_Page' );
}
add_action( 'widgets_init', 'osvn_load_child_page_widget' );
?>