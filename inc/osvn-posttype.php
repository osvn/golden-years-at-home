<?php 
add_action('init','osvn_client_post_type');
	function osvn_client_post_type() {

		$labels = array(

		    'name' => 'Slider',

		    'singular_name' => 'Slider',

		    'add_new' => 'Add Slider',

		    'all_items' => 'Slider',

		    'add_new_item' => 'Add Slider',

		    'edit_item' => 'Edit Slider',

		    'new_item' => 'Add Slider',

		    'view_item' => 'View Slider',

		    'search_items' => 'Search Slider',

		    'not_found' =>  'Not found Slider',

		    'not_found_in_trash' => 'Not found Slider in trash',

		    'parent_item_colon' => 'Parent Slider: ',

		    'menu_name' => 'Slider'

		);

		

		$args = array(

			'labels' => $labels,

			'description' => "",

			'public' => true,

			'exclude_from_search' => false,

			'publicly_queryable' => true,

			'show_ui' => true, 

			'show_in_nav_menus' => true, 

			'show_in_menu' => true,

			'show_in_admin_bar' => true,

			//'menu_position' => 100,

			'menu_icon' => 'dashicons-images-alt2',

			//'capability_type' => 'post',

			'hierarchical' => false,

			'supports' => array('title', 'thumbnail'),

			'has_archive' => false,

			//'rewrite'   => array( 'slug' => 'p-showroom' ),

			'query_var' => true,

			'can_export' => true

		); 

		

		register_post_type('osvn_slide',$args);

	}
	/////////////
	register_taxonomy("product_brand", 
		array('product'), 
		array(	"hierarchical" => true, 
		"label" => "Brand", 
		"singular_label" => "Brand", 
		'rewrite'   => array( 'slug' => 'product-brand' ),
		"query_var" => true
	));
	register_taxonomy("product_gender", 
		array('product'), 
		array(	"hierarchical" => true, 
		"label" => "Gender", 
		"singular_label" => "Gender", 
		'rewrite'   => array( 'slug' => 'product-gender' ),
		"query_var" => true
	));
?>