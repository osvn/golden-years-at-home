<?php
//https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress/wiki
function osvn_metaboxes( $meta_boxes ) {
    $prefix = '_osvn_'; // Prefix for all fields
    /////////////////////////////////////////
    $tt_pages = array();
    $tt_pages_obj = get_pages('sort_column=post_parent,menu_order');    
    foreach ($tt_pages_obj as $tt_page) {
    $tt_pages[$tt_page->ID] = $tt_page->post_title; }
    /////////////////////////////////////////
    $tt_terms = array();
    $tt_terms_obj = get_terms('faq_cat');    
    foreach ($tt_terms_obj as $tt_term) {
    $tt_terms[$tt_term->slug] = $tt_term->name; }
    
    
    
    
    
    
    
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_home',
        'title' => __( 'Home box bottom', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'Template-home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                        'name'    => 'Box title',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home_box_title',
                        'type'    => 'text',
                    ),
                    
                    
                    array(
                        'name' => 'Image',
                        'desc' => 'Upload an image or enter an URL.',
                        'id' => $prefix . 'home_box_image',
                        'type' => 'file',
                        'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                    ),
                    array(
                        'name'    => 'Box description',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home_box_des',
                        'type'    => 'textarea_code',
                        //'options'  => $tt_pages,
                    ),
            
            
            
        ),
    );
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_slider',
        'title' => __( 'Slider data', 'osvn' ),
        'pages' => array('osvn_slide'), // post type
        //'show_on' => array( 'key' => 'page-template', 'value' => 'Template-contact.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                'name' => 'Link',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'slider_link',
                'type' => 'text'
            ),
            
            
            
            
        ),
    );
    
    
    
    
    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'osvn_metaboxes' );