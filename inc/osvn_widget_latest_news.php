<?php 
// Creating the widget 
class OSVN_Widget_Latest_News extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'OSVN_Widget_Latest_News', 

// Widget name will appear in UI
__('* OSVN Widget Latest News', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'OSVN widget latest news', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
?>

	<?php 
	$osvn_args = array('post_type' => 'post', 'posts_per_page'=>5);
	$osvn_wq = new WP_Query($osvn_args);
	if($osvn_wq->have_posts()):
		echo '<div>';
		while($osvn_wq->have_posts()): $osvn_wq->the_post();
	?>
			<div class="blog_recent_post_list_item_container">
				<a href="<?php the_permalink();?>">
					<?php the_post_thumbnail('widget-thumb', array('class'=>'blog_recent_post_list_thumb'));?>
				</a>
				<div class="blog_recent_post_list_title">
					<a href="<?php the_permalink();?>" class="blog_recent_post_list_title_link"><?php the_title();?></a>
				</div>
				<div class="blog_recent_post_list_summary">
					<p><?php echo wp_trim_words(get_the_content(), 30, '. ');?></p>
				</div>
				<div class="blog_recent_post_list_bottom_links">
					<a href="<?php the_permalink();?>" class="blog_recent_post_list_read_more">Read More &raquo;</a>
				</div>
			</div>
	<?php 
		endwhile;
		echo '</div>';
	endif;
	wp_reset_query();
	?>
<?php
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Latest News', 'osvn' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function osvn_load_latest_news_widget() {
	register_widget( 'OSVN_Widget_Latest_News' );
}
add_action( 'widgets_init', 'osvn_load_latest_news_widget' );
?>