<?php 
/*
Template name: Contact
*/
?>
<?php get_header();?>
<div id="main" class="contact">
		<div class="container">
			<div class="content content-single after-clear">
				<?php while(have_posts()): the_post();?>
				<div class="text-content">
					<h3 class="title"><?php single_post_title();?></h3>
				</div>
				<?php the_content();?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
<?php get_footer();?>