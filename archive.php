<?php
global $osvn_opt;
get_header();
echo '
<div class="full">
    <div class="container">
        <div id="index">
';
				if ( have_posts() ) // Neu co bai viet
				{
					echo '<h1 class="page-title">';
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'osvn' ), get_the_date() );

					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'osvn' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'osvn' ) ) );

					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'osvn' ), get_the_date( _x( 'Y', 'yearly archives date format', 'osvn' ) ) );

					elseif ( is_category() ) :
						printf( __( 'Category Archives: %s', 'osvn' ), single_cat_title( '', false ) );

					elseif ( is_tag() ) :
						printf( __( 'Tag Archives: %s', 'osvn' ), single_tag_title( '', false ) );

					elseif ( is_author() ) :
						printf( __( 'All posts by %s', 'osvn' ), get_the_author() );

					elseif ( is_search() ) :
						printf( __( 'Search Results for: %s', 'osvn' ), get_search_query() );

					else :
						_e( 'Archives', 'osvn' );

					endif;
					echo '</h1>';


					while ( have_posts() ) : the_post();

						get_template_part( 'content' );

					endwhile;

				} else { // Neu khong co bai viet

					get_template_part( 'content', 'none' );

				}
echo '</div></div></div>';
get_footer();