<?php get_header();?>
<?php 
global $post;
?>
<div id="main" class="full">
    <div class="container">
        <div class="content <?php if(is_page('about')){echo 'about';}else{echo 'content-single';}?>">
            <?php while(have_posts()): the_post();?>
            <?php if(is_page('about')){the_post_thumbnail('about-thumb', array('class'=>'thumb res-img'));}?>
            <div class="text-content">
                <h3 class="title"><?php the_title();?></h3>
                <?php the_content();?>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</div>
<?php get_footer();?>