<?php
global $osvn_opt;
?>
<footer>
		<div class="container relative">
			<div class="after-clear">
				<?php
					// Main Menu
					wp_nav_menu(array(
						'theme_location' => 'secondary',
						'menu_class' => 'fl menu',
						'container' => false,
						'menu_id' => 'footer-menu',
					));
				?>
				<div class="copyright fr">Copyright &copy; <?php echo date('Y');?> <a href="<?php echo esc_url(home_url());?>">GoldenYearsAtHome.com</a> All Rights Reserved. Website Design by <a href="http://creativehaus.com/" target="_blank">CreativeHaus</a></div>
			</div>
		</div>
    </footer>
    <?php 
	global $current_user;
    get_currentuserinfo();
	if($current_user->ID == ''){
	?>
	<div id="login" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<?php wc_get_template( 'myaccount/form-login.php' ); ?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /#login -->
	<?php }?>

	<div id="your-cart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<?php 
					global $woocommerce;
					if ( sizeof( $woocommerce->cart->cart_contents ) > 0 ) {
						echo '<h2>Your cart.</h2>';
						wc_get_template( 'cart/cart.php' );
					}else{
					?>
					<h2 class="cart-empty">Your cart is currently empty.</h2>
					<?php }?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /#Cart -->
</div>
<?php wp_footer();?>
<script>
jQuery('.wpcf7-form br').remove();
jQuery('.wpcf7-form').addClass('fl');
</script>
<script><?php if(isset($osvn_opt['footer_script']) && !empty($osvn_opt['footer_script'])){echo $osvn_opt['footer_script'];} ?></script>
</body>
</html>
